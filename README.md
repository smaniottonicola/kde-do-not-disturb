# kde-do-not-disturb
`kde-do-not-disturb` is an mpv plugin that disables the notifications of KDE Plasma. This only works on Linux and is tested while running KDE Plasma, but it can probably work with other DEs since it uses the freedesktop dbus. Mpv must be compiled with cplugin support.

## Build
Run `make`, the included Makefile should do the rest.

## Install
Run `make install` to automatically copy the object file in `$HOME/.config/mpv/scripts`, which will enable it for your user.
