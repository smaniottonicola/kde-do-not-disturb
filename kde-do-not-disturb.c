#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include <QtDBus>

#include <mpv/client.h>

void enable_dnd(){
	QDBusInterface iface(
		"org.freedesktop.Notifications",
		"/org/freedesktop/Notifications",
		"org.freedesktop.Notifications",
		QDBusConnection::sessionBus()
	);

	iface.call("Inhibit", "mpv", "Playing a video", QVariantMap());
}

extern "C"{
	int mpv_open_cplugin(mpv_handle *handle){
		enable_dnd();

		while (true)
		{
			mpv_event *event = mpv_wait_event(handle, 1);
			if (event->event_id == MPV_EVENT_SHUTDOWN)
				break;
		}
		return 0;
	}
}
